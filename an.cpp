#include "graph.h"
#include "DEBUG.h"
#include "timing.hpp"
using namespace std;
#include <random>
#include <cmath>
#include <cfloat>
#include <algorithm>
#include <set>
#define N 55
#define S 0
#define T (N - 1)
#define K 34
#define NUM_ITER 10

Graph G;
double TEMPERATURE  = 1000.0;
#define COOL_RATE 0.96
random_device rd;
static mt19937 eng(rd());
static uniform_real_distribution<> randFloat(0.0, 1.0);
string CSVFILE(){
    return "undirected_weighted_graph" + to_string(N)+".csv";
 }
int rand_btw(int m, int n){
    if((n - m) <= 0){
        cout<<(((m - n) == 0) ? "equal \n":"minus\n");
        return -1;
    }
    uniform_int_distribution<> mn(m, n);
    return mn(eng);
}
int get_tour_length(Graph& G, Tour& t){
    int cost = 0;
    for(int i = 1; i < t.size(); i++){
        vertex u = t[i-1], v = t[i];
        cost += G[u][v];
        if(v == T) break;
    }
    return cost;
}
Tour greedy(Tour& trace, vector<bool>& visited, vertex cur, int step){
    // vector<vertex> go;
    visited[cur] = true;
    trace.push_back(cur);
    if(step == 2){
        trace.push_back(T);
        return trace;
    }
    vertex go;
    int min_edge_cost = INF;
    for(int i = 0; i < N; i++){
        if(!visited[i] && G[cur][i] < min_edge_cost){
            go = i;
            min_edge_cost = G[cur][i];
        }
    }
    return greedy(trace, visited, go, step - 1);
}
Tour solution;
int minCost;
Tour get_a_tour(){
    // cout<<"get a tour\n";
    Tour sol;
    for(int i = 1; i <= T - 1; i++)
        sol.push_back(i);
    random_shuffle(sol.begin(), sol.end());
    sol.insert(sol.begin(), S);
    int Tpos = rand_btw(K - 1, N - 2);
    // PRINT_TOKEN(Tpos)
    sol.insert(sol.begin() + Tpos, T);

    return sol; 
}

void init(int L){
    vector<Tour> candidates;
    solution = get_a_tour();
    int a, b;
    for(int i = 1; i < L; i++){
        Tour tmp = get_a_tour();
        a = get_tour_length(G, tmp);
        b = get_tour_length(G, solution);
        if(a < b){
            solution = tmp;
            minCost = a;
        }
    }
}
void greedy_init(){
    solution = {};
    auto visited = vector<bool>(N, false);
    visited[T] = true;
    solution =  greedy(solution ,visited, S, K );
    minCost = get_tour_length(G, solution);
    vector<bool> in_solution = vector<bool>(N, false);
    for(vertex& v:solution) in_solution[v] = true;
    vector<int> foobar;
    for(int i = 0; i < N; i++)
        if(!in_solution[i])
            foobar.push_back(i);
    random_shuffle(foobar.begin(), foobar.end());
    for(int& i : foobar)
        solution.push_back(i);
}
Tour get_neighbor(const Tour& origin){
    Tour t = origin;
    int tpos = 0;
    tpos = 0; while(t[tpos] != T) tpos++;
    int i1 = rand_btw(1, tpos), i2;
    //如果要交换终点T，保证T不会过早出现：至少在k个点之后才会出现
    if(i1 == tpos){
        do{
            i2 = rand_btw(K - 1, N - 1);
        }while(i1 == i2);
    }else{
        while(true){
            i2 = rand_btw(1, N - 1);
            if(i2 != i1 && i2 != tpos) break;
        }
    }
    swap(t[i1], t[i2]);
    tpos = 0; while(t[tpos] != T) tpos++;
    return t;
}

void boot(){
    // init(10);

    // int foo = 0;
    // while(solution[foo] != T) foo++;
    #ifndef APPLY_GREEDY
    init(4096);
    #else
    greedy_init();

    #endif
    int kk = 0;
    while(TEMPERATURE > DBL_MIN){
        ++kk;
        vector<Tour> neigbors;
        for(int i = 0; i < 1024; i++)
            neigbors.push_back(get_neighbor(solution));
        Tour* bestneighbor; int tmpcost = INF;
            for(Tour& nei : neigbors){
                if(get_tour_length(G, nei) < tmpcost){
                    tmpcost = get_tour_length(G, nei);
                    bestneighbor = &nei;
                }
            }
        if(tmpcost <= minCost){
            solution = *bestneighbor;
            minCost = tmpcost;
        }
        else{
            double rndflt = randFloat(eng);
            auto posib = exp((double)(minCost - tmpcost)/TEMPERATURE);
            // posib *= 0.;
            if(posib > rndflt){
                solution = *bestneighbor;
                minCost = tmpcost;
            }
        }
        TEMPERATURE *= COOL_RATE;   
    }
    
}
int main(){
    // cout<<"Loading Graph from csv...\n";
    G = read_from_csv(N, CSVFILE());
    // print2console(G);
    // cout<<"Load complete.\n";
    TIMER_START(x);
    boot();
    cout<<minCost<<endl;   
    greedy_init();
    cout<<get_tour_length(G, solution)<<endl;
    TIMER_STOP(x);
    cout<<"time elpased :"<<TIMER_SEC(x)<<endl;
}