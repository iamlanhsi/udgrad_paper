#include "graph.h"
#include "DEBUG.h"
#include <algorithm>
using namespace std;
#include <cfloat>
// #include <time.h>
#include "timing.hpp"
#define N 55
#define S 0
#define T (N - 1)
#define K 34
// #define CSV_FILE "undirected_weighted_graph89.csv"
#define POP_SIZE 1000
#define ITERATION 200
#define P_cross 0.9
#define P_mutate 0.2
#define APPLY_GREEDY yes
#define DO_ELITE_MAINTAIN YES
Graph G;
string CSVFILE(){
    return "undirected_weighted_graph" + to_string(N)+".csv";
 }
typedef vector<int> chromosome;
static random_device rd;
static mt19937 eng(rd());
static uniform_real_distribution<> unireal(0.0, 1.0);
Tour greedy(Tour& trace, vector<bool>& visited, vertex cur, int step){
    // vector<vertex> go;
    visited[cur] = true;
    trace.push_back(cur);
    if(step == 2){
        trace.push_back(T);
        return trace;
        }
    vertex go;
    int min_edge_cost = INF;
    for(int i = 0; i < N; i++){
        if(!visited[i] && G[cur][i] < min_edge_cost){
            go = i;
            min_edge_cost = G[cur][i];
        }
    }
    return greedy(trace, visited, go, step - 1);
}

int get_tour_length(Graph& G, Tour& t){
    int cost = 0;
    for(int i = 1; i < N; i++){
        vertex u = t[i-1], v = t[i];
        cost += G[u][v];
        if(v == T) break;
    }
    return cost;
}
int rand_btw(int m, int n){
    if((n - m) <= 0){
        cout<<(((m - n) == 0) ? "equal \n":"minus\n");
        return 0;
    }

    uniform_int_distribution<> mn(m, n);
    return mn(eng);
}
double get_fitness(chromosome& c){
    int cost = get_tour_length(G, c);
    // if(cost == 0) cout<<"err cost = 0\n";
    return 1000.0/(double)cost;
}
bool contains(vector<int>& v, int n){
    for(int& i : v) if(i == n) return true;
    return false;
}
class individual{
    public:
    chromosome chrome;
    double fitness;
    individual(){
        //[!S....!T....]
        for (int i = 1; i < N - 1; i++)
            chrome.push_back(i);
        random_shuffle(chrome.begin(), chrome.end());
        chrome.insert(chrome.begin(), S);
        int pos = rand_btw(K, N) - 1;
        //k  = 2, n = 5
        //[3,45,1,0,9]
        chrome.insert(chrome.begin() + pos, T);
        fitness = get_fitness(chrome);
    }
    individual(chromosome& CR){
        chrome = CR;
        fitness = get_fitness(CR);
    }
    individual& operator=(const individual& another){
        this->chrome = another.chrome;
        this->fitness = another.fitness;
        return *this;
    }   
    //找到终点之前的某个点，变异该点
    void mutate(){
        int pos = 0;
        while(chrome[pos] != T)
            pos++; //[1, 2, 3, 4, 5, 6, 7] , where index = 2, val = 
        int where = rand_btw(1, pos - 1);
        int origin = chrome[where];
        int mutval = rand_btw(1, N - 2);
        int tmp = 0;
        while(chrome[tmp] != mutval) tmp++;
        swap(chrome[where], chrome[tmp]);
        if(chrome.size() < N) cout<<"mutation err\n";
        fitness = get_fitness(chrome);
    }

    pair<individual, individual> operator *(individual& ma) {
        //excluding the origin point.
        vector<int> c1 = vector<int>(chrome.begin() + 1, chrome.end());
        vector<int> c2 = vector<int>(ma.chrome.begin() + 1, ma.chrome.end());

        int tpos1 = 0, tpos2 = 0;
        while(c1[tpos1] != T) tpos1 ++;
        while(c2[tpos2] != T) tpos2 ++;
        //[2,4,3,9,6,7] tpos = 5
        int x = rand_btw(K / 2, min(tpos1, tpos2) - 1);
        int y = rand_btw(0, (K / 2) - 1);

        vector<int> selected_gene;
        for(int i = y; i <= x; i++)
            selected_gene.push_back(c1[i]);
        vector<int> sel_gene_in_c2;
        for(int& g : selected_gene){
            for(int i = 0; i < c2.size(); i++)
                if(c2[i] == g) 
                    sel_gene_in_c2.push_back(i);
        }
        sort(sel_gene_in_c2.begin(), sel_gene_in_c2.end());
        for(int i = 0; i < selected_gene.size(); i++){
            swap(c1[y + i], c2[sel_gene_in_c2[i]]);
        }
        c1.insert(c1.begin(), S); c2.insert(c2.begin(), S);
        return pair<individual, individual>(c1, c2);
}
};
individual the_greedy_one(){
    auto visited = vector<bool>(N, false);
    visited[T] = true;
    chromosome gc;
    gc = greedy(gc, visited, S, K);
    vector<bool> in_solution = vector<bool>(N, false);
    for(vertex& v:gc) in_solution[v] = true;
    vector<int> foobar;
    for(int i = 0; i < N; i++)
        if(!in_solution[i])
            foobar.push_back(i);
    random_shuffle(foobar.begin(), foobar.end());
    for(int& i : foobar)
        gc.push_back(i);
    return individual(gc);
}
int roulette_wheel(vector<individual>& pop){
    int npop = pop.size();
    vector<double> vd;
    for(auto& id : pop)
        vd.push_back(id.fitness);
    double sumfitness = 0.0;
    for(double& f : vd)
        sumfitness += f;
    //let vd normalized.
    for(double& f : vd)
        f /= sumfitness;

    double randreal = unireal(eng);
    int i = 0;
    for(; i < npop; i++){
        randreal -= vd[i];
        if(randreal < DBL_MIN)
             return i;
    }
    
    cout<<"roulette wheel failed.\n";
    return rand_btw(0, npop - 1);
}
void natrual_select(vector<individual>& originPop){
    vector<individual> newPop;
    for(int i = 0; i < POP_SIZE; i++){
        int id = roulette_wheel(originPop);
        // int id = rand_btw(0, originPop.size() - 1);
        newPop.push_back(originPop[id]);
    }
    originPop = newPop;
}

void elite_maintain(vector<individual>& pop, individual& elite){
    int worst_id = 0;
    double worst_fitness = DBL_MAX;
    int n = pop.size();
    for(int i = 0; i < n; i++){
        if(pop[i].fitness < worst_fitness){
            worst_fitness = pop[i].fitness;
            worst_id = i;
        }
    }
    pop[worst_id] = elite;
}
Tour opti_tour = {};
double opti_val = INT_MAX;
vector<individual> population;
void boot(){
    for (int i = 0; i < POP_SIZE; i++)
        population.push_back(individual());
    #ifdef APPLY_GREEDY
        population[0] = the_greedy_one();
    #endif
    for(int cnt = 0; cnt < ITERATION; cnt++){  
        int best_id;

        double foobar = population[0].fitness;
        for(int j = 1; j < population.size(); j++){
            if(population[j].fitness > foobar){
                foobar = population[j].fitness;
                best_id = j;
            }
        }
        auto& best = population[best_id];
        individual copy_of_best;
        copy_of_best = population[best_id];
        int tmp_min = get_tour_length(G, best.chrome);
        if(tmp_min < opti_val){
            opti_tour = best.chrome;
            opti_val = tmp_min;
        }
        natrual_select(population);
        #ifdef DO_ELITE_MAINTAIN
            elite_maintain(population, copy_of_best);
        #endif
        double do_crossover = unireal(eng);
        // if(do_crossover <= P_cross){
        for(int cnt = 0; cnt < (int)(POP_SIZE * P_cross) / 2; cnt ++){
            int faid = rand_btw(0, POP_SIZE - 2), maid = faid+1;

            auto child = individual();
            auto childs = population[faid]*(population[maid]);
            auto son = childs.first;
            auto girl = childs.second;
            double do_mutate1 = unireal(eng);
            double do_mutate2 = unireal(eng);
            if(do_mutate1 <= P_mutate) son.mutate();
            if(do_mutate2 <= P_mutate) girl.mutate();
            population.push_back(son);
            population.push_back(girl);
        }
        // }
    }
}
int main(){

    // cout<<"Loading Graph from csv...\n";
    G = read_from_csv(N, CSVFILE());
    // cout<<"Load complete.\n";
    TIMER_START(x);
    boot();
    TIMER_STOP(x);
    cout<<opti_val<<endl;
    cout<<"time elapsed: "<<TIMER_SEC(x)<<"s. \n";
    print_tour(opti_tour);
}