#include "graph.h"
int write_to_csv(Graph g ,string filename){

	ofstream outFile;
	outFile.open(filename, ios::out); // 打开模式可省略
    int n = g.size();
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            if(g[i][j] != 0){
                outFile<<i<<','<<j<<','<<g[i][j]<<endl;
            }
        }
    }
	outFile.close();
    return 0;
}
Graph read_from_csv(int n, string filename = "savefile_myGraph1.csv")
{    
	// 读文件
    auto res = vector<vector<int> >(n, vector<int>(n, 0));
	ifstream inFile(filename, ios::in);
	string lineStr;
	vector<vector<string>> strArray;
	while (getline(inFile, lineStr))
	{
		// 打印整行字符串
		// cout << lineStr << endl;
		// 存成二维表结构
		stringstream ss(lineStr);
		string str;
		vector<string> lineArray;
		// 按照逗号分隔
        int i = 0;
		while (getline(ss, str, ','))
            // cout<<str<<endl;
			lineArray.push_back(str);
            // G[]
		strArray.push_back(lineArray);
	}
    // getchar();
	inFile.close();
    for(vector<string>& line : strArray){
        int edge[3];
        for(int i = 0; i < 3; i++)
            edge[i] =  atoi(line[i].c_str()); 
        int u = edge[0], v = edge[1], w = edge[2];
        // cout<<u<<v<<w<<endl;
        res[u][v] = w;
    }
	return res;
}
Graph gen_random_graph(int n){
    // cout<<"pos = "<< possib<<endl;
    random_device rd;
    default_random_engine eng(rd());
    normal_distribution<> uniint(100, 50);
    // static normal_distribution<double> distr(15, 15);
    // static uniform_int_distribution<> wegiht(3,15);
    auto G = vector<vector<int> >(n, vector<int>(n, 0));
    for(int i = 0; i < n; i++)
        for(int j = i + 1; j < n; j++)
            {
                int randomweight = lround(uniint(eng));
                G[i][j] = randomweight;
                G[j][i] = randomweight;
            }
            return G;
}
void print2console(Graph& G){
    for(auto& line : G)
    {
        for(auto& i : line) cout<<(i == INF ? 0 : i)<<" ";
        cout<<endl;
    }
}
void print_tour(Tour& v){
    for(int& i :v) cout<<i<<",";
    cout<<endl;
}