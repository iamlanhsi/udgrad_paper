#ifndef GRAPH
#define GRAPH
#include <cmath>
#include <random>
#include <chrono>
#include <vector>
#include <climits>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#define INF INT_MAX
using namespace std;
typedef vector<vector<int> > Graph;
// #define FIN freopen( "inputs.txt", "r", stdin)
// using namespace std;
typedef unsigned long long ull;
typedef vector<int> Tour;
typedef int vertex;
Graph read_from_csv(int n, string filename);
int write_to_csv(Graph g , string filename);
Graph gen_random_graph(int);
void print2console(Graph& G);
void print_tour(Tour& t);
// int get_tour_length(Graph& G, Tour& t);
#endif