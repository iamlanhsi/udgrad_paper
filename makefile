.PHONY:all
all:an aco ga 
an : an.o graph.o
	g++ -o an graph.o an.o -std=c++11
an.o : graph.h an.cpp
	g++ -c an.cpp -std=c++11
aco:ACO.o graph.o
	g++ -o aco ACO.o graph.o -std=c++11
ACO.o:ACO.cpp graph.h DEBUG.h
	g++  -c ACO.cpp -std=c++11
random_graph_gen: graph.o GEN_RANDOM.CPP
	g++ -o random_graph_gen graph.o GEN_RANDOM.CPP -std=c++11
ga : genetic_algo.o graph.o
	g++ -o ga graph.o genetic_algo.o -std=c++11
genetic_algo.o : graph.h genetic_algo.cpp
	g++ -c genetic_algo.cpp -std=c++11
graph.o : graph.cpp graph.h
	g++ -c graph.cpp -std=c++11