#include <iostream>
#include <vector>
#include <set>
#include <random>
#include "graph.h"   
#include <algorithm>
#include <stdlib.h>
#include "DEBUG.h"
#define DFS_INIT\
    DFS_path = Tour(), visited = vector<bool>(N_NODES, false), DFSRESULT = Tour();
using namespace std;
typedef unsigned long long ull;
#define N_ITER 100
#define SIZE_TABU_TABLE 10
#define TABU_DURATION 50
#define K 55
#define NGHB_SIZE 4

typedef pair<int, int> Pii;
int flag;
Graph G;
Tour DFS_path, DFSRESULT;//Note : this variable is used for DFS() onlu!!
vector<bool> visited;
vector<Pii> tabu_table;
Tour opti_solution;
int opti_cost;
void shuffle(vector<int>& v){
    int n = v.size();
    for(int i = 0; i < 10; i++){
        int x1 = random(n);
        int x2 = random(n);
        swap(v[x1], v[x2]);
    }
}

bool in_table(Tour t){
    int len = get_tour_length(G, t);
    for(auto& e : tabu_table) 
        if(e.first <= len) return true;
    return false;
}
void DFS(vertex s, vertex t){
    DFS_path.push_back(s);
    visited[s] = true;

    if(s == t){
        DFSRESULT = DFS_path;
        return ;
    }
    vector<vertex> go;
    for(int i = 0; i < N_NODES; i++){
        if(G[s][i] != 0 && !visited[i])
            go.push_back(i);
    }  
    if(!go.empty())
        shuffle(go);

    for(vertex& v : go)
        DFS(v, t);

    DFS_path.pop_back();
}
Tour get_neighbor(Tour cur){
    // breakpoint
    // cout<<"get neighbor\n";
    int n = cur.size();
    int randum = -1;
    do{
        randum = random(n);
    }while(randum < 1 || randum >= n - 1); // 1 - n-2

    vertex ns = cur[randum];

    Tour ngbr = {};
    for(int i = 0; i < randum - 1; i++){
        ngbr.push_back(cur[i]);
    }
    DFS_INIT for(int vt :ngbr) visited[vt] = true;
    DFS(ns, T);
    // breakpoint
    for(vertex& v : DFSRESULT) ngbr.push_back(v);
    // cout<<"returning\n";
    return ngbr;
}
void update_tabu_table(){
            //duration -= 1
        for(Pii& p : tabu_table) p.second--;

        vector<pair<int, int> > v;
        for(auto& p : tabu_table){
            if(p.second != 0)
                v.push_back(p);
        }
        tabu_table = v;
}
void tabu_search(){
    tabu_table = vector<Pii >();
    while(opti_solution.size() < K){
        DFS_INIT DFS(S, T); // generate a random path as init
        opti_solution = DFSRESULT;
    }
    opti_cost = get_tour_length(G, opti_solution);
    print_tour(opti_solution);
    tabu_table.push_back(Pii(opti_cost, TABU_DURATION));

    int n_iter  = N_ITER;
    while(n_iter--){
        // cout<<"round"<<n_iter<<endl;
        vector<Tour> neighbors;
        //get neighboors.  
        for(int i = 0; i < NGHB_SIZE; i++){
            Tour tmp = get_neighbor(opti_solution);
            while(tmp.size() < K) tmp = get_neighbor(opti_solution);

            if(tmp.size() >= K)
                neighbors.push_back(tmp);
        }
        //find out best neighborhood.
        PRINT_TOKEN(neighbors.size())
        auto best_candi = neighbors[0];
        auto best_candi_val = INF;
        for(auto& candi : neighbors){
            if(!in_table(candi) && get_tour_length(G, candi) < best_candi_val){
                best_candi = candi;
                best_candi_val = get_tour_length(G, candi);
            }
        }
        // breakpoint
        //if all candidates are in tabu table.
        //compare best candidate with the current optimal
        if(best_candi_val < opti_cost){
            opti_solution = best_candi;
            opti_cost = best_candi_val;
        }
        //if the table is filled, first element is erased.
        if(tabu_table.size() == SIZE_TABU_TABLE)
            tabu_table.erase(tabu_table.begin());
        
        tabu_table.push_back(Pii(best_candi_val, TABU_DURATION));

        update_tabu_table();
    }
}

int main(){
    // FEED;
    cout<<"Loading Graph from csv...\n";
    G = read_from_csv(89, "sparse_savefile_myGraph4.csv");
    // print2console(G);
    cout<<"Load complete.\n";
    tabu_search();
    cout<<"optimal solution = "<<opti_cost<<endl;
    for(vertex& v : opti_solution) cout<<v<<" ";
    cout<<endl;
    return 0;
}